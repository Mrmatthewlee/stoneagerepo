﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    /**
* Class Name: WritingWorkshop <br>
* Class Purpose: Implement the GainResource method for the Writing Workshop location where the player gains user stories <br>
*
* <hr>
* Date created: 6/19/2020 <br>
* @author Darien Roach
*/
    class WritingWorkshop : ILocation
    {
        public int DeveloperCount { get; set; }
        public int RESOURCE_DIVISOR { get; private set; }

        /**
 * Method Name: WritingWorkshop<br>
 * Method Purpose: Default constructor for the WritingWorkshop class, collecting the developer count from the player and establishing the resource divisor constant <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @param  developerCount - used to instantiated the value of the class's own DeveloperCount property
 */
        public WritingWorkshop(int developerCount)
        {
            DeveloperCount = developerCount;
            RESOURCE_DIVISOR = 2;
        }

        /**
* Method Name: GainResource<br>
* Method Purpose: Returns the amount of User Stories the player obtains after randomly generating values between 1-6 and summing the total <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @return total amount of user stories gained
*/
        public int GainResource(int experienceBonus)
        {
            int userStories;
            int sum = experienceBonus;

            Random rng = new Random();

            for (int i = 0; i < DeveloperCount; i++)
            {
                sum += rng.Next(1, 7);
            }

            userStories = sum / RESOURCE_DIVISOR;

            return userStories;
        }
    }
}
