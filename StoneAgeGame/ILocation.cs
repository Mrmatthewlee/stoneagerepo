﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    /**
* Class Name: ILocation <br>
* Class Purpose: Act as an interface connecting the the major resource locations to the Player through a GainResource method<br>
*
* <hr>
* Date created: 6/19/2020 <br>
* @author Darien Roach
*/
    interface ILocation
    {
        int GainResource(int experienceBonus);
    }
}
