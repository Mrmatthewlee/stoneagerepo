﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    /**
* Class Name: MarketAnalysis <br>
* Class Purpose: Implement the GainResource method for the Market Analysis location where the player gains constraints<br>
*
* <hr>
* Date created: 6/19/2020 <br>
* @author Darien Roach
*/
    class MarketAnalysis : ILocation
    {
        public int DeveloperCount { get; set; }
        public int RESOURCE_DIVISOR { get; private set; }

        /**
 * Method Name: MarketAnalysis<br>
 * Method Purpose: Default constructor for the MarketAnalysis class, collecting the developer count from the player and establishing the resource divisor constant <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @param  developerCount - used to instantiated the value of the class's own DeveloperCount property
 */
        public MarketAnalysis(int developerCount)
        {
            DeveloperCount = developerCount;
            RESOURCE_DIVISOR = 4;
        }

        /**
* Method Name: MarketAnalysis<br>
* Method Purpose: Returns the amount of Constraints the player obtains after randomly generating values between 1-6 and summing the total <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @return total amount of constraints gained
*/
        public int GainResource(int experienceBonus)
        {
            int constraints;
            int sum = experienceBonus;

            Random rng = new Random();

            for (int i = 0; i < DeveloperCount; i++)
            {
                sum += rng.Next(1, 7);
            }

            constraints = sum / RESOURCE_DIVISOR;

            return constraints;
        }

    }
}
