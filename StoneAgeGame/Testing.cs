﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    /**
   * Class Name: Testing <br>
   * Class Purpose: Implement the GainResource method for the Testing location where the player gains error handling<br>
   *
   * <hr>
   * Date created: 6/19/2020 <br>
   * @author Darien Roach
*/
    class Testing : ILocation
    {
        public int DeveloperCount { get; set; }
        public int RESOURCE_DIVISOR { get; private set; }

        /**
 * Method Name: Testing<br>
 * Method Purpose: Default constructor for the Testing class, collecting the developer count from the player and establishing the resource divisor constant <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @param  developerCount - used to instantiated the value of the class's own DeveloperCount property
 */
        public Testing(int developerCount)
        {
            DeveloperCount = developerCount;
            RESOURCE_DIVISOR = 6;
        }

        /**
 * Method Name: GainResource<br>
 * Method Purpose: Returns the amount of Error Handling the player obtains after randomly generating values between 1-6 and summing the total  <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @return total amount of error handling gained
 */
        public int GainResource(int experienceBonus)
        {
            int errorHandling;
            int sum = experienceBonus;

            Random rng = new Random();

            for (int i = 0; i < DeveloperCount; i++)
            {
                sum += rng.Next(1, 7);
            }

            errorHandling = sum / RESOURCE_DIVISOR;

            return errorHandling;
        }
    }
}
