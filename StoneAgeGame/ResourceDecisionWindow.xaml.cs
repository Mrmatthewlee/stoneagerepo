﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace StoneAgeGame
{
    /// <summary>
    /// Interaction logic for ResourceDecisionWindow.xaml
    /// </summary>
    public partial class ResourceDecisionWindow : Window
    {

        string windowPurpose;
        int position;
        Deliverable deliverable;
        Feature feature;

        //Pass a new player to this popup window via constructor
        public ResourceDecisionWindow(int resourcesRequired, int availableExperience, Deliverable deliverable, int position, Feature feature, string _windowPurpose)
        {
            InitializeComponent();
            windowPurpose = _windowPurpose;
            this.deliverable = deliverable;
            this.feature = feature;
            this.position = position;

            WindowSetup(resourcesRequired, availableExperience);
        }


        public void WindowSetup(int resourcesRequired, int availableExperience)
        {


            if (windowPurpose == "feeding")
            {
                DescriptionLabel.Content = "You do not have enough user stories to give to your developers.&#xD;&#xA;You must spend another resource.";
                ResourcesNeededLabel.Content = resourcesRequired.ToString();

                ToolsToApply.Opacity = 0;
                ToolsAppliedLabel.Opacity = 0;
            }
            else if (windowPurpose == "tools")
            {
                //Make irrelevant labels magically invisible
                ResourcesNeededLabel.Content = availableExperience.ToString();
                ResourcesNeededTextLabel.Content = "Available Experience: ";
                QualitiesLabel.Opacity = 0;
                ConstraintsLabel.Opacity = 0;
                InterafacesLabel.Opacity = 0;
                ErrorHandlingLabel.Opacity = 0;
                _qualitiesSpent.Opacity = 0;
                _constraintsSpent.Opacity = 0;
                _interfacesSpent.Opacity = 0;
                _errorHandlingSpent.Opacity = 0;

                DescriptionLabel.Content = "How many experiences (tools) would you like to apply?";
            }
            else if (windowPurpose == "deliverable")
            {
                DescriptionLabel.Content = "What resources will you expend for this deliverable card?";

                ResourcesNeededTextLabel.Content = "Cost: ";
                ResourcesNeededLabel.Content = $"{4 - position} of any resource";

                ToolsToApply.Opacity = 0;
                ToolsAppliedLabel.Opacity = 0;

                MainWindow.player1.GainedCard = false;
                MainWindow.player2.GainedCard = false;
            }
            else if (windowPurpose == "feature")
            {
                DescriptionLabel.Content = "What resources will you expend for this feature card?";

                ResourcesNeededTextLabel.Content = "Cost: ";

                switch (feature.FeatureType)
                {
                    case ("Normal"):
                        ResourcesNeededLabel.Content = $"{feature.QualityCost} qual, {feature.ConstraintCost} con, {feature.InterfaceCost} int, {feature.ErrorHandlingCost} err";
                        break;
                    case ("Any"):
                        ResourcesNeededLabel.Content = $"{feature.GeneralCost} of any {feature.NumberDifferent} different resources";
                        break;
                    case ("Range"):
                        ResourcesNeededLabel.Content = $"{feature.MinimumResources} to {feature.MaximumResources} of any resources";
                        break;
                }

                ToolsToApply.Opacity = 0;
                ToolsAppliedLabel.Opacity = 0;

                MainWindow.player1.GainedCard = false;
                MainWindow.player2.GainedCard = false;
            }
        }

        [STAThread]
        private void ConfirmationButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (windowPurpose == "feeding")
            {
                DialogResult dlg;

                int qualitiesSpent = 0;
                int constraintSpent = 0;
                int interfaceSpent = 0;
                int errorHandlingSpent = 0;

                bool valid = true;
                bool player1Turn = MainWindow.isPlayer1Turn;

                int.TryParse(_qualitiesSpent.Text, out qualitiesSpent);
                int.TryParse(_constraintsSpent.Text, out constraintSpent);
                int.TryParse(_interfacesSpent.Text, out interfaceSpent);
                int.TryParse(_errorHandlingSpent.Text, out errorHandlingSpent);

                int sum = qualitiesSpent + constraintSpent + interfaceSpent + errorHandlingSpent;
                int requiredResources = 0;

                int.TryParse(ResourcesNeededLabel.Content.ToString(), out requiredResources);

                if(sum < requiredResources)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough resources have been expended, will you proceed?", "Too Low", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if(dlg == System.Windows.Forms.DialogResult.Yes)
                    {
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else if(sum > requiredResources)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "You are spending more resources than necessary", "Too High", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }

                if (qualitiesSpent < 0 || constraintSpent < 0 || interfaceSpent < 0 || errorHandlingSpent < 0)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "No negative resource amounts", "No Negatives", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }

                if (valid)
                {
                    if (player1Turn)
                    {
                        if(MainWindow.player1.qualities < qualitiesSpent || MainWindow.player1.constraints < constraintSpent || MainWindow.player1.interfaces < interfaceSpent || MainWindow.player1.errorHandling < errorHandlingSpent)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Player 1, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }
                        else
                        {
                            valid = true;
                        }
                    }
                    else
                    {
                        if (MainWindow.player2.qualities < qualitiesSpent || MainWindow.player2.constraints < constraintSpent || MainWindow.player2.interfaces < interfaceSpent || MainWindow.player2.errorHandling < errorHandlingSpent)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Player 2, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }
                        else
                        {
                            valid = true;
                        }
                    }
                }

                if (valid)
                {
                    if (MainWindow.isPlayer1Turn)
                    {
                        MainWindow.player1.SpendFeedingResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);
                        Close();
                    }
                    else
                    {
                        MainWindow.player2.SpendFeedingResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);
                        Close();
                    }
                }
               
            }
            else if (windowPurpose == "tools")
            {
                if (MainWindow.isPlayer1Turn)
                {
                    int numExperience = 0;
                    int.TryParse(ToolsToApply.Text, out numExperience);
                    if(numExperience <= MainWindow.player1.AvailableExperience)
                    {
                        MainWindow.player1.ExperienceToBeApplied = numExperience;
                        Close();
                    }
                }
                else
                {
                    int numExperience = 0;
                    int.TryParse(ToolsToApply.Text, out numExperience);
                    if (numExperience <= MainWindow.player2.AvailableExperience)
                    {
                        MainWindow.player2.ExperienceToBeApplied = numExperience;
                        Close();
                    }
                }
                    
            }
            else if (windowPurpose == "deliverable")
            {
                DialogResult dlg;

                int qualitiesSpent = 0;
                int constraintSpent = 0;
                int interfaceSpent = 0;
                int errorHandlingSpent = 0;

                int.TryParse(_qualitiesSpent.Text, out qualitiesSpent);
                int.TryParse(_constraintsSpent.Text, out constraintSpent);
                int.TryParse(_interfacesSpent.Text, out interfaceSpent);
                int.TryParse(_errorHandlingSpent.Text, out errorHandlingSpent);

                bool valid = true;
                bool paid = true;

                int sum = qualitiesSpent + constraintSpent + interfaceSpent + errorHandlingSpent;

                int cost = (4 - position);

                if (sum < cost)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough resources have been expended, will you proceed? (Note: proceeding will not allow you to gain the card)", "Too Low", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (dlg == System.Windows.Forms.DialogResult.Yes)
                    {
                        valid = true;
                        paid = false;
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else if (sum > cost)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "You are spending more resources than necessary", "Too High", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }

                if (qualitiesSpent < 0 || constraintSpent < 0 || interfaceSpent < 0 || errorHandlingSpent < 0)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "No negative resource amounts", "No Negatives", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }

                if (valid)
                {
                    if (MainWindow.isPlayer1Turn)
                    {
                        if (MainWindow.player1.qualities < qualitiesSpent || MainWindow.player1.constraints < constraintSpent || MainWindow.player1.interfaces < interfaceSpent || MainWindow.player1.errorHandling < errorHandlingSpent)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Player 1, you do not have enough resources for this allocation", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }
                        else
                        {
                            valid = true;
                        }
                    }
                    else
                    {
                        if (MainWindow.player2.qualities < qualitiesSpent || MainWindow.player2.constraints < constraintSpent || MainWindow.player2.interfaces < interfaceSpent || MainWindow.player2.errorHandling < errorHandlingSpent)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Player 2, you do not have enough resources for this allocation", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }
                        else
                        {
                            valid = true;
                        }
                    }
                }

                if (valid && paid)
                {
                    if (MainWindow.isPlayer1Turn)
                    {
                        MainWindow.player1.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                        switch (deliverable.ResourceReward)
                        {
                            case ("Qualities"):
                                MainWindow.player1.qualities += deliverable.RewardAmount;
                                break;
                            case ("Constraints"):
                                MainWindow.player1.constraints += deliverable.RewardAmount;
                                break;
                            case ("Interfaces"):
                                MainWindow.player1.interfaces += deliverable.RewardAmount;
                                break;
                            case ("Error Handling"):
                                MainWindow.player1.errorHandling += deliverable.RewardAmount;
                                break;
                        }

                        MainWindow.player1.score += deliverable.PointValue;

                        MainWindow.player1.GainedCard = true;

                        Close();
                    }
                    else
                    {
                        MainWindow.player2.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                        switch (deliverable.ResourceReward)
                        {
                            case ("Qualities"):
                                MainWindow.player2.qualities += deliverable.RewardAmount;
                                break;
                            case ("Constraints"):
                                MainWindow.player2.constraints += deliverable.RewardAmount;
                                break;
                            case ("Interfaces"):
                                MainWindow.player2.interfaces += deliverable.RewardAmount;
                                break;
                            case ("Error Handling"):
                                MainWindow.player2.errorHandling += deliverable.RewardAmount;
                                break;
                        }

                        MainWindow.player2.score += deliverable.PointValue;

                        MainWindow.player2.GainedCard = true;

                        Close();
                    }
                }
                else
                {
                    if (MainWindow.isPlayer1Turn)
                    {
                        MainWindow.player1.GainedCard = false;
                    }
                    else
                    {
                        MainWindow.player2.GainedCard = false;
                    }

                    Close();
                }
                
            }
            else if (windowPurpose == "feature")
            {
                DialogResult dlg;

                int qualitiesSpent = 0;
                int constraintSpent = 0;
                int interfaceSpent = 0;
                int errorHandlingSpent = 0;

                int.TryParse(_qualitiesSpent.Text, out qualitiesSpent);
                int.TryParse(_constraintsSpent.Text, out constraintSpent);
                int.TryParse(_interfacesSpent.Text, out interfaceSpent);
                int.TryParse(_errorHandlingSpent.Text, out errorHandlingSpent);

                bool valid = true;
                bool paid = true;

                int numDiffResourcesSpent = 0;

                int sum = qualitiesSpent + constraintSpent + interfaceSpent + errorHandlingSpent;

                if (qualitiesSpent < 0 || constraintSpent < 0 || interfaceSpent < 0 || errorHandlingSpent < 0)
                {
                    dlg = System.Windows.Forms.MessageBox.Show(null, "No negative resource amounts", "No Negatives", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    valid = false;
                }

                switch (feature.FeatureType)
                {
                    case ("Normal"):
                        if(sum < feature.TOTAL_COST)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough resources have been expended, will you proceed? (Note: proceeding will not allow you to gain the card)", "Too Low", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if (dlg == System.Windows.Forms.DialogResult.Yes)
                            {
                                valid = true;
                                paid = false;
                            }
                            else
                            {
                                valid = false;
                            }
                        }

                        if (sum > feature.TOTAL_COST)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "You are spending more resources than necessary", "Too High", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }

                        if (valid)
                        {
                            if(qualitiesSpent < feature.QualityCost)
                            {
                                dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough qualities have been spent, will you proceed? (Note: proceeding will not allow you to gain the card)", "Not Enough", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                if (dlg == System.Windows.Forms.DialogResult.Yes)
                                {
                                    valid = true;
                                    paid = false;
                                }
                                else
                                {
                                    valid = false;
                                }
                            }

                            if (constraintSpent < feature.ConstraintCost)
                            {
                                dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough constraints have been spent, will you proceed? (Note: proceeding will not allow you to gain the card)", "Not Enough", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                if (dlg == System.Windows.Forms.DialogResult.Yes)
                                {
                                    valid = true;
                                    paid = false;
                                }
                                else
                                {
                                    valid = false;
                                }
                            }

                            if (interfaceSpent < feature.InterfaceCost)
                            {
                                dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough interfaces have been spent, will you proceed? (Note: proceeding will not allow you to gain the card)", "Not Enough", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                if (dlg == System.Windows.Forms.DialogResult.Yes)
                                {
                                    valid = true;
                                    paid = false;
                                }
                                else
                                {
                                    valid = false;
                                }
                            }

                            if (errorHandlingSpent < feature.ErrorHandlingCost)
                            {
                                dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough error handling has been spent, will you proceed? (Note: proceeding will not allow you to gain the card)", "Not Enough", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                if (dlg == System.Windows.Forms.DialogResult.Yes)
                                {
                                    valid = true;
                                    paid = false;
                                }
                                else
                                {
                                    valid = false;
                                }
                            }

                            if(valid)
                            {
                                if (MainWindow.isPlayer1Turn)
                                {
                                    if (MainWindow.player1.qualities < qualitiesSpent || MainWindow.player1.constraints < constraintSpent || MainWindow.player1.interfaces < interfaceSpent || MainWindow.player1.errorHandling < errorHandlingSpent)
                                    {
                                        dlg = System.Windows.Forms.MessageBox.Show(null, "Player 1, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        valid = false;
                                    }
                                    else
                                    {
                                        valid = true;
                                    }
                                }
                                else
                                {
                                    if (MainWindow.player2.qualities < qualitiesSpent || MainWindow.player2.constraints < constraintSpent || MainWindow.player2.interfaces < interfaceSpent || MainWindow.player2.errorHandling < errorHandlingSpent)
                                    {
                                        dlg = System.Windows.Forms.MessageBox.Show(null, "Player 2, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        valid = false;
                                    }
                                    else
                                    {
                                        valid = true;
                                    }
                                }
                            }

                            if(valid && paid)
                            {
                                if (MainWindow.isPlayer1Turn)
                                {
                                    MainWindow.player1.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                    MainWindow.player1.score += feature.PointValue;

                                    MainWindow.player1.GainedCard = true;

                                    Close();
                                }
                                else
                                {
                                    MainWindow.player2.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                    MainWindow.player2.score += feature.PointValue;

                                    MainWindow.player2.GainedCard = true;

                                    Close();
                                }

                            }
                            else
                            {
                                if (MainWindow.isPlayer1Turn)
                                {
                                    MainWindow.player1.GainedCard = false;
                                }
                                else
                                {
                                    MainWindow.player2.GainedCard = false;
                                }
                                Close();
                            }
                        }
                        break;
                    case ("Any"):
                        if (sum < feature.TOTAL_COST)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough resources have been expended, will you proceed? (Note: proceeding will not allow you to gain the card)", "Too Low", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if (dlg == System.Windows.Forms.DialogResult.Yes)
                            {
                                valid = true;
                                paid = false;
                            }
                            else
                            {
                                valid = false;
                            }
                        }

                        if (sum > feature.TOTAL_COST)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "You are spending more resources than necessary", "Too High", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }

                        if (valid)
                        {
                            if(qualitiesSpent > 0)
                            {
                                numDiffResourcesSpent++;
                            }
                            if(constraintSpent > 0)
                            {
                                numDiffResourcesSpent++;
                            }
                            if(interfaceSpent > 0)
                            {
                                numDiffResourcesSpent++;
                            }
                            if(errorHandlingSpent > 0)
                            {
                                numDiffResourcesSpent++;
                            }

                            if(numDiffResourcesSpent < feature.NumberDifferent)
                            {
                                dlg = System.Windows.Forms.MessageBox.Show(null, "You need to spend more different resources", "Not Enough Uniqueness", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                valid = false;
                            }
                        }

                        if (valid)
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                if (MainWindow.player1.qualities < qualitiesSpent || MainWindow.player1.constraints < constraintSpent || MainWindow.player1.interfaces < interfaceSpent || MainWindow.player1.errorHandling < errorHandlingSpent)
                                {
                                    dlg = System.Windows.Forms.MessageBox.Show(null, "Player 1, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    valid = false;
                                }
                                else
                                {
                                    valid = true;
                                }
                            }
                            else
                            {
                                if (MainWindow.player2.qualities < qualitiesSpent || MainWindow.player2.constraints < constraintSpent || MainWindow.player2.interfaces < interfaceSpent || MainWindow.player2.errorHandling < errorHandlingSpent)
                                {
                                    dlg = System.Windows.Forms.MessageBox.Show(null, "Player 2, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    valid = false;
                                }
                                else
                                {
                                    valid = true;
                                }
                            }
                        }

                        if(valid && paid)
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                MainWindow.player1.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                feature.QualityCost = qualitiesSpent;
                                feature.ConstraintCost = constraintSpent;
                                feature.InterfaceCost = interfaceSpent;
                                feature.ErrorHandlingCost = errorHandlingSpent;

                                feature.CalculatePointValue();

                                MainWindow.player1.score += feature.PointValue;

                                MainWindow.player1.GainedCard = true;

                                Close();
                            }
                            else
                            {
                                MainWindow.player2.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                feature.QualityCost = qualitiesSpent;
                                feature.ConstraintCost = constraintSpent;
                                feature.InterfaceCost = interfaceSpent;
                                feature.ErrorHandlingCost = errorHandlingSpent;

                                feature.CalculatePointValue();

                                MainWindow.player2.score += feature.PointValue;

                                MainWindow.player2.GainedCard = true;

                                Close();
                            }
                        }
                        else
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                MainWindow.player1.GainedCard = false;
                            }
                            else
                            {
                                MainWindow.player2.GainedCard = false;
                            }
                            
                            Close();
                        }
                        break;
                    case ("Range"):
                        if (sum < feature.MinimumResources)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "Not enough resources have been expended, will you proceed? (Note: proceeding will not allow you to gain the card)", "Too Low", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if (dlg == System.Windows.Forms.DialogResult.Yes)
                            {
                                valid = true;
                                paid = false;
                            }
                            else
                            {
                                valid = false;
                            }
                        }

                        if (sum > feature.MaximumResources)
                        {
                            dlg = System.Windows.Forms.MessageBox.Show(null, "You are spending more resources than allowed", "Too High", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            valid = false;
                        }

                        if (valid)
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                if (MainWindow.player1.qualities < qualitiesSpent || MainWindow.player1.constraints < constraintSpent || MainWindow.player1.interfaces < interfaceSpent || MainWindow.player1.errorHandling < errorHandlingSpent)
                                {
                                    dlg = System.Windows.Forms.MessageBox.Show(null, "Player 1, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    valid = false;
                                }
                                else
                                {
                                    valid = true;
                                }
                            }
                            else
                            {
                                if (MainWindow.player2.qualities < qualitiesSpent || MainWindow.player2.constraints < constraintSpent || MainWindow.player2.interfaces < interfaceSpent || MainWindow.player2.errorHandling < errorHandlingSpent)
                                {
                                    dlg = System.Windows.Forms.MessageBox.Show(null, "Player 2, you do not have enough resources for this allocation, try again", "Invalid Amounts", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    valid = false;
                                }
                                else
                                {
                                    valid = true;
                                }
                            }
                        }

                        if (valid && paid)
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                MainWindow.player1.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                feature.QualityCost = qualitiesSpent;
                                feature.ConstraintCost = constraintSpent;
                                feature.InterfaceCost = interfaceSpent;
                                feature.ErrorHandlingCost = errorHandlingSpent;

                                feature.CalculatePointValue();

                                MainWindow.player1.score += feature.PointValue;

                                MainWindow.player1.GainedCard = true;

                                Close();
                            }
                            else
                            {
                                MainWindow.player2.SpendGeneralResources(qualitiesSpent, constraintSpent, interfaceSpent, errorHandlingSpent);

                                feature.QualityCost = qualitiesSpent;
                                feature.ConstraintCost = constraintSpent;
                                feature.InterfaceCost = interfaceSpent;
                                feature.ErrorHandlingCost = errorHandlingSpent;

                                feature.CalculatePointValue();

                                MainWindow.player2.score += feature.PointValue;

                                MainWindow.player2.GainedCard = true;

                                Close();
                            }
                        }
                        else
                        {
                            if (MainWindow.isPlayer1Turn)
                            {
                                MainWindow.player1.GainedCard = false;
                            }
                            else
                            {
                                MainWindow.player2.GainedCard = false;
                            }

                            Close();
                        }

                        break;
                }
            }
        }
    }
}
