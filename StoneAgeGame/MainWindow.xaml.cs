﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace StoneAgeGame
{
    /// <summary>
    /// All methods in this class are for the buttons and labels.
    /// 
    /// </summary>
    public partial class MainWindow : Window
    {
        GameManager gameManager = new GameManager();
        DeckHandler deckHandler = new DeckHandler();
        List<Deliverable> deliverableDisplay = new List<Deliverable>(4);
        List<Feature> featureDisplay = new List<Feature>(2);
        //Since we only have 2 players and their 
        public static Player player1 = new Player();
        public static Player player2 = new Player();

        public static bool isPlayer1Turn;

        public MainWindow()
        {
            InitializeComponent();
            //Player 1 starts the game
            isPlayer1Turn = true;

            for(int i = 0; i < 4; i++)
            {
                deliverableDisplay.Add(null);
            }

            for (int i = 0; i < 2; i++)
            {
                featureDisplay.Add(null);
            }

            //Debug
            //player2.qualities = 99;
            //player2.constraints = 99;
            //player2.interfaces = 99;
            //player2.errorHandling = 99;

            //player1.qualities = 99;
            //player1.constraints = 99;
            //player1.interfaces = 99;
            //player1.errorHandling = 99;

            GenerateInitialDeck();
        }

        #region Resource Buttons
        private void FoodButton_Click(object sender, RoutedEventArgs e)
        {
            string location = "WritingWorkshop";
            GameFlowDecision(location);
        }

        private void WoodButton_Click(object sender, RoutedEventArgs e)
        {
            string location = "Interviews";
            GameFlowDecision(location);
        }

        private void ClayButton_Click(object sender, RoutedEventArgs e)
        {
            string location = "MarketAnalysis";
            GameFlowDecision(location);
        }

        private void StoneButton_Click(object sender, RoutedEventArgs e)
        {
            string location = "Prototyping";
            GameFlowDecision(location);
        }

        private void GoldButton_Click(object sender, RoutedEventArgs e)
        {
            string location = "Testing";
            GameFlowDecision(location);
        }



        #endregion



        #region Location Buttons
        //Buttons for the 3 locations
        private void FarmButton_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if(FarmButton.Background != Brushes.Red)
                {
                    
                    string location = "Business";
                    GameFlowDecision(location);
                }
            }
                
            else
            {
                if(FarmButton.Background != Brushes.Blue)
                {
                    
                    string location = "Business";
                    GameFlowDecision(location);
                }
                
            }
                
        }

        private void HutButton_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if(HutButton.Background != Brushes.Red)
                {
                    
                    string location = "HiringFirm";
                    GameFlowDecision(location);
                }
               
            }
            else
            {
                if(HutButton.Background != Brushes.Blue)
                {
                    
                    string location = "HiringFirm";
                    GameFlowDecision(location);
                }
                
            }
                
        }

        private void D1Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (D1Button.Background != Brushes.Red)
                {

                    string location = "D1";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (D1Button.Background != Brushes.Blue)
                {

                    string location = "D1";
                    GameFlowDecision(location);
                }

            }
        }

        private void D2Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (D2Button.Background != Brushes.Red)
                {

                    string location = "D2";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (D2Button.Background != Brushes.Blue)
                {

                    string location = "D2";
                    GameFlowDecision(location);
                }

            }
        }

        private void D3Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (D3Button.Background != Brushes.Red)
                {

                    string location = "D3";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (D3Button.Background != Brushes.Blue)
                {

                    string location = "D3";
                    GameFlowDecision(location);
                }

            }
        }

        private void D4Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (D4Button.Background != Brushes.Red)
                {

                    string location = "D4";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (D4Button.Background != Brushes.Blue)
                {

                    string location = "D4";
                    GameFlowDecision(location);
                }

            }
        }

        private void Stack1Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (Stack1Button.Background != Brushes.Red)
                {

                    string location = "Stack1";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (Stack1Button.Background != Brushes.Blue)
                {

                    string location = "Stack1";
                    GameFlowDecision(location);
                }

            }
        }

        private void Stack2Button_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if (Stack2Button.Background != Brushes.Red)
                {

                    string location = "Stack2";
                    GameFlowDecision(location);
                }

            }
            else
            {
                if (Stack2Button.Background != Brushes.Blue)
                {

                    string location = "Stack2";
                    GameFlowDecision(location);
                }

            }
        }

        private void ToolsButton_Click(object sender, RoutedEventArgs e)
        {
            if (isPlayer1Turn)
            {
                if(ToolsButton.Background != Brushes.Red)
                {
                    
                    string location = "TrainingWorkshop";
                    GameFlowDecision(location);
                }
                
            }
                
            else
            {
                if(ToolsButton.Background != Brushes.Blue)
                {
                    
                    string location = "TrainingWorkshop";
                    GameFlowDecision(location);
                }
                
            }
                
        }

        #endregion


        #region Game Control Buttons
        //Button for next phase
        private void NextStateButton_Click(object sender, RoutedEventArgs e)
        {
            switch (gameManager.states[gameManager.currentState])
            {
                case ("Planning"):
                    ManagePlanningTurns();
                    break;
                case ("Execution"):
                    ManageExecutionTurns();
                    break;
                case ("Review"):
                    ManageReviewTurns();
                    break;
            }
            TurnLabel.Content = $"Turn: {gameManager.turnNumber}";  //Update turn counter
            player1.IsLocked = false;
            player2.IsLocked = false;

            player1.HasPlaced = false;
            player2.HasPlaced = false;

            player1.HasRetrieved = false;
            player2.HasRetrieved = false;

            if (isPlayer1Turn)
            {
                PlayerLabel.Content = "Current Player: Player 1";
            }
            else
            {
                PlayerLabel.Content = "Current Player: Player 2";
            }

            UpdateBoard();
        }

        public void ManagePlanningTurns()
        {
            if ((isPlayer1Turn && player1.HasPlaced) || ((!isPlayer1Turn) && player2.HasPlaced))
            {
                if (player1.workersAvailable > 0 || player2.workersAvailable > 0)
                {
                    if (player1.workersAvailable == 0)
                    {
                        isPlayer1Turn = false;
                        StateLabel.Content = "Player 2 Planning";
                        gameManager.currentState = 1;
                        gameManager.NextTurn();
                    }
                    else if (player2.workersAvailable == 0)
                    {
                        isPlayer1Turn = true;
                        StateLabel.Content = "Player 1 Planning";
                        gameManager.currentState = 0;
                        gameManager.NextTurn();
                    }
                    else
                    {
                        if (isPlayer1Turn)
                        {
                            StateLabel.Content = "Player 2 Planning";           //Update phase title label
                            isPlayer1Turn = false;
                            gameManager.currentState = 1;
                            gameManager.NextTurn();
                        }
                        else
                        {
                            StateLabel.Content = "Player 1 Planning";           //Update phase title label
                            isPlayer1Turn = true;
                            gameManager.currentState = 0;
                            gameManager.NextTurn();
                        }
                    }
                }
                else
                {
                    gameManager.NextPhase();
                    gameManager.NextTurn();
                    StateLabel.Content = $"Player 1 {gameManager.states[gameManager.currentState]}";
                    isPlayer1Turn = true;
                }
            }
        }

        public void ManageExecutionTurns()
        {
            if ((isPlayer1Turn && player1.HasRetrieved) || ((!isPlayer1Turn) && player2.HasRetrieved))
            {
                if (player1.workersAvailable < player1.totalWorkers || player2.workersAvailable < player2.totalWorkers)
                {
                    if (player1.workersAvailable == player1.totalWorkers)
                    {
                        isPlayer1Turn = false;
                        StateLabel.Content = "Player 2 Execution";
                        gameManager.currentState = 3;
                        gameManager.NextTurn();
                    }
                    else if (player2.workersAvailable == player2.totalWorkers)
                    {
                        isPlayer1Turn = true;
                        StateLabel.Content = "Player 1 Execution";
                        gameManager.currentState = 2;
                        gameManager.NextTurn();
                    }
                    else
                    {
                        if (isPlayer1Turn)
                        {
                            StateLabel.Content = "Player 2 Execution";           //Update phase title label
                            isPlayer1Turn = false;
                            gameManager.currentState = 3;
                            gameManager.NextTurn();
                        }
                        else
                        {
                            StateLabel.Content = "Player 1 Execution";           //Update phase title label
                            isPlayer1Turn = true;
                            gameManager.currentState = 2;
                            gameManager.NextTurn();
                        }
                    }
                }
                else
                {
                    gameManager.NextPhase();
                    gameManager.NextTurn();
                    StateLabel.Content = $"Player 1 {gameManager.states[gameManager.currentState]}";
                    isPlayer1Turn = true;
                }
            }
        }

        public void ManageReviewTurns()
        {
            SendPlayerReview();
            isPlayer1Turn = !isPlayer1Turn;

            if (isPlayer1Turn)
            {
                gameManager.NextPhase();
                gameManager.NextTurn();
                StateLabel.Content = $"Player 1 {gameManager.states[gameManager.currentState]}";
                isPlayer1Turn = true;
            }
            else
            {
                gameManager.NextTurn();
            }
        }
        #endregion


        #region Helpers
        List<Deliverable> deliverables = new List<Deliverable>();

        public void GenerateInitialDeck()
        {

            for(int i = 3; i > -1; i--)
            {
                deliverableDisplay[i] = deckHandler.DrawDeliverable();
            }

            featureDisplay[0] = deckHandler.DrawFeature(1);
            featureDisplay[1] = deckHandler.DrawFeature(2);

            UpdateBoard();
        }

        

        //This is the method designed to control what the resource buttons do.
        //The idea is to reuse buttons to keep controls simplified for the user.
        public void GameFlowDecision(string location)
        {
            if (gameManager.states[gameManager.currentState] == "Planning")
            {
                SendWorkerAssignment(location);
            }
            else if (gameManager.states[gameManager.currentState] == "Execution")
            {
                SendWorkerPulls(location);
            }
            else if (gameManager.states[gameManager.currentState] == "Review")
            {
                //SendPlayerReview();

            }
            UpdateBoard();
        }

        //Assign workers by passing the location name
        public void SendWorkerAssignment(string location)
        {
            if (isPlayer1Turn && player1.workersAvailable > 0)
            {
                if (player1.IsLocked)
                {
                    if(location == player1.CurrentLocation)
                    {
                        switch (location)
                        {
                            case ("Business"):
                                if(FarmButton.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    FarmButton.Background = Brushes.Blue;
                                }
                                break;
                            case ("HiringFirm"):
                                if(HutButton.Background != Brushes.Blue && player1.workersAvailable > 1)
                                {
                                    player1.workersAvailable -= 2;
                                    player1.HasPlaced = true;
                                    HutButton.Background = Brushes.Blue;
                                }
                                break;
                            case ("TrainingWorkshop"):
                                if(ToolsButton.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    ToolsButton.Background = Brushes.Blue;
                                }
                                break;
                            case ("D1"):
                                if (D1Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    D1Button.Background = Brushes.Blue;
                                }
                                break;
                            case ("D2"):
                                if (D2Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    D2Button.Background = Brushes.Blue;
                                }
                                break;
                            case ("D3"):
                                if (D3Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    D3Button.Background = Brushes.Blue;
                                }
                                break;
                            case ("D4"):
                                if (D4Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    D4Button.Background = Brushes.Blue;
                                }
                                break;
                            case ("Stack1"):
                                if (Stack1Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    Stack1Button.Background = Brushes.Blue;
                                }
                                break;
                            case ("Stack2"):
                                if (Stack2Button.Background != Brushes.Blue)
                                {
                                    player1.workersAvailable -= 1;
                                    player1.HasPlaced = true;
                                    Stack2Button.Background = Brushes.Blue;
                                }
                                break;
                            default:
                                player1.AssignWorker(location);
                                player1.HasPlaced = true;
                                break;
                        }
                    }
                }
                else
                {
                    switch (location)
                    {
                        case ("Business"):
                            if (FarmButton.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                FarmButton.Background = Brushes.Blue;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                            }
                            break;
                        case ("HiringFirm"):
                            if (HutButton.Background != Brushes.Blue && player1.workersAvailable > 1)
                            {
                                player1.workersAvailable -= 2;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                HutButton.Background = Brushes.Blue;
                            }
                            break;
                        case ("TrainingWorkshop"):
                            if (ToolsButton.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                ToolsButton.Background = Brushes.Blue;
                            }
                            break;
                        case ("D1"):
                            if (D1Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D1Button.Background = Brushes.Blue;
                            }
                            break;
                        case ("D2"):
                            if (D2Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D2Button.Background = Brushes.Blue;
                            }
                            break;
                        case ("D3"):
                            if (D3Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D3Button.Background = Brushes.Blue;
                            }
                            break;
                        case ("D4"):
                            if (D4Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D4Button.Background = Brushes.Blue;
                            }
                            break;
                        case ("Stack1"):
                            if (Stack1Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                Stack1Button.Background = Brushes.Blue;
                            }
                            break;
                        case ("Stack2"):
                            if (Stack2Button.Background != Brushes.Blue)
                            {
                                player1.workersAvailable -= 1;
                                player1.HasPlaced = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                Stack2Button.Background = Brushes.Blue;
                            }
                            break;
                        default:
                            player1.AssignWorker(location);
                            player1.IsLocked = true;
                            player1.CurrentLocation = location;
                            player1.HasPlaced = true;
                            break;
                    }
                   
                }
            }         
            else if (!isPlayer1Turn && player2.workersAvailable > 0)
            {
                if (player2.IsLocked)
                {
                    if (location == player2.CurrentLocation)
                    {
                        switch (location)
                        {
                            case ("Business"):
                                if (FarmButton.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    FarmButton.Background = Brushes.Red;
                                }
                                break;
                            case ("HiringFirm"):
                                if (HutButton.Background != Brushes.Red && player2.workersAvailable > 1)
                                {
                                    player2.workersAvailable -= 2;
                                    player2.HasPlaced = true;
                                    HutButton.Background = Brushes.Red;
                                }
                                break;
                            case ("TrainingWorkshop"):
                                if (ToolsButton.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    ToolsButton.Background = Brushes.Red;
                                }
                                break;
                            case ("D1"):
                                if (D1Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    D1Button.Background = Brushes.Red;
                                }
                                break;
                            case ("D2"):
                                if (D2Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    D2Button.Background = Brushes.Red;
                                }
                                break;
                            case ("D3"):
                                if (D3Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    D3Button.Background = Brushes.Red;
                                }
                                break;
                            case ("D4"):
                                if (D4Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    D4Button.Background = Brushes.Red;
                                }
                                break;
                            case ("Stack1"):
                                if (Stack1Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    Stack1Button.Background = Brushes.Red;
                                }
                                break;
                            case ("Stack2"):
                                if (Stack2Button.Background != Brushes.Red)
                                {
                                    player2.workersAvailable -= 1;
                                    player2.HasPlaced = true;
                                    Stack2Button.Background = Brushes.Red;
                                }
                                break;
                            default:
                                player2.AssignWorker(location);
                                player2.HasPlaced = true;
                                break;
                        }
                    }
                }
                else
                {
                    switch (location)
                    {
                        case ("Business"):
                            if (FarmButton.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                FarmButton.Background = Brushes.Red;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                            }
                            break;
                        case ("HiringFirm"):
                            if (HutButton.Background != Brushes.Red && player2.workersAvailable > 1)
                            {
                                player2.workersAvailable -= 2;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                HutButton.Background = Brushes.Red;
                            }
                            break;
                        case ("TrainingWorkshop"):
                            if (ToolsButton.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                ToolsButton.Background = Brushes.Red;
                            }
                            break;
                        case ("D1"):
                            if (D1Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D1Button.Background = Brushes.Red;
                            }
                            break;
                        case ("D2"):
                            if (D2Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D2Button.Background = Brushes.Red;
                            }
                            break;
                        case ("D3"):
                            if (D3Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D3Button.Background = Brushes.Red;
                            }
                            break;
                        case ("D4"):
                            if (D4Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D4Button.Background = Brushes.Red;
                            }
                            break;
                        case ("Stack1"):
                            if (Stack1Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                Stack1Button.Background = Brushes.Red;
                            }
                            break;
                        case ("Stack2"):
                            if (Stack2Button.Background != Brushes.Red)
                            {
                                player2.workersAvailable -= 1;
                                player2.HasPlaced = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                Stack2Button.Background = Brushes.Red;
                            }
                            break;
                        default:
                            player2.AssignWorker(location);
                            player2.IsLocked = true;
                            player2.CurrentLocation = location;
                            player2.HasPlaced = true;
                            break;
                    }
                }
            }
        }

        //Pull workers by passing location name
        public void SendWorkerPulls(string location)
        {
            if (isPlayer1Turn && (player1.workerLocation[location] > 0 || location == "Business" || location == "HiringFirm" || location == "TrainingWorkshop" || location == "D1" || location == "D2" || location == "D3" || location == "D4" || location == "Stack1" || location == "Stack2") )
            {
                if (player1.IsLocked)
                {
                    if (location == player1.CurrentLocation)
                    {
                        switch (location)
                        {
                            case ("Business"):
                                if (FarmButton.Background != Brushes.Transparent)
                                {
                                    player1.Agriculture += 1;
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    FarmButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("HiringFirm"):
                                if (HutButton.Background != Brushes.Transparent)
                                {
                                    player1.totalWorkers += 1;
                                    player1.workersAvailable += 3;
                                    player1.HasRetrieved = true;
                                    HutButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("TrainingWorkshop"):
                                if (ToolsButton.Background != Brushes.Transparent)
                                {
                                    player1.Experience += 1;
                                    player1.AvailableExperience += 1;
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    ToolsButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D1"):
                                if (D1Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(0);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    D1Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D2"):
                                if (D2Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(1);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    D2Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D3"):
                                if (D3Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(2);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    D3Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D4"):
                                if (D4Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(3);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    D4Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("Stack1"):
                                if (Stack1Button.Background != Brushes.Transparent)
                                {
                                    ParseFeature(0);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    Stack1Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("Stack2"):
                                if (Stack2Button.Background != Brushes.Transparent)
                                {
                                    ParseFeature(1);
                                    player1.workersAvailable += 1;
                                    player1.HasRetrieved = true;
                                    Stack2Button.Background = Brushes.Transparent;
                                }
                                break;
                            default:
                                player1.PullWorkers(location);
                                player1.HasRetrieved = true;
                                break;
                        }
                    }
                }
                else
                {
                    switch (location)
                    {
                        case ("Business"):
                            if (FarmButton.Background != Brushes.Transparent)
                            {
                                player1.Agriculture += 1;
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                FarmButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("HiringFirm"):
                            if (HutButton.Background != Brushes.Transparent)
                            {
                                player1.totalWorkers += 1;
                                player1.workersAvailable += 3;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                HutButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("TrainingWorkshop"):
                            if (ToolsButton.Background != Brushes.Transparent)
                            {
                                player1.Experience += 1;
                                player1.AvailableExperience += 1;
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                ToolsButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D1"):
                            if (D1Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(0);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D1Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D2"):
                            if (D2Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(1);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D2Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D3"):
                            if (D3Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(2);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D3Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D4"):
                            if (D4Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(3);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                D4Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("Stack1"):
                            if (Stack1Button.Background != Brushes.Transparent)
                            {
                                ParseFeature(0);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                Stack1Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("Stack2"):
                            if (Stack2Button.Background != Brushes.Transparent)
                            {
                                ParseFeature(1);
                                player1.workersAvailable += 1;
                                player1.HasRetrieved = true;
                                player1.IsLocked = true;
                                player1.CurrentLocation = location;
                                Stack2Button.Background = Brushes.Transparent;
                            }
                            break;
                        default:
                            player1.PullWorkers(location);
                            player1.HasRetrieved = true;
                            player1.IsLocked = true;
                            player1.CurrentLocation = location;
                            break;
                    }
                }
            }
            else if (!isPlayer1Turn && (player2.workerLocation[location] > 0 || location == "Business" || location == "HiringFirm" || location == "TrainingWorkshop" || location == "D1" || location == "D2" || location == "D3" || location == "D4" || location == "Stack1" || location == "Stack2") )
            {
                if (player2.IsLocked)
                {
                    if (location == player2.CurrentLocation)
                    {
                        switch (location)
                        {
                            case ("Business"):
                                if (FarmButton.Background != Brushes.Transparent)
                                {
                                    player2.Agriculture += 1;
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    FarmButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("HiringFirm"):
                                if (HutButton.Background != Brushes.Transparent)
                                {
                                    player2.totalWorkers += 1;
                                    player2.workersAvailable += 3;
                                    player2.HasRetrieved = true;
                                    HutButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("TrainingWorkshop"):
                                if (ToolsButton.Background != Brushes.Transparent)
                                {
                                    player2.Experience += 1;
                                    player2.AvailableExperience += 1;
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    ToolsButton.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D1"):
                                if (D1Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(0);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    D1Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D2"):
                                if (D2Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(1);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    D2Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D3"):
                                if (D3Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(2);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    D3Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("D4"):
                                if (D4Button.Background != Brushes.Transparent)
                                {
                                    ParseDeliverable(3);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    D4Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("Stack1"):
                                if (Stack1Button.Background != Brushes.Transparent)
                                {
                                    ParseFeature(0);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    Stack1Button.Background = Brushes.Transparent;
                                }
                                break;
                            case ("Stack2"):
                                if (Stack2Button.Background != Brushes.Transparent)
                                {
                                    ParseFeature(1);
                                    player2.workersAvailable += 1;
                                    player2.HasRetrieved = true;
                                    Stack2Button.Background = Brushes.Transparent;
                                }
                                break;
                            default:
                                player2.PullWorkers(location);
                                player2.HasRetrieved = true;
                                break;
                        }
                    }
                }
                else
                {
                    switch (location)
                    {
                        case ("Business"):
                            if (FarmButton.Background != Brushes.Transparent)
                            {
                                player2.Agriculture += 1;
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                FarmButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("HiringFirm"):
                            if (HutButton.Background != Brushes.Transparent)
                            {
                                player2.totalWorkers += 1;
                                player2.workersAvailable += 3;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                HutButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("TrainingWorkshop"):
                            if (ToolsButton.Background != Brushes.Transparent)
                            {
                                player2.Experience += 1;
                                player2.AvailableExperience += 1;
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                ToolsButton.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D1"):
                            if (D1Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(0);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D1Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D2"):
                            if (D2Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(1);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D2Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D3"):
                            if (D3Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(2);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D3Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("D4"):
                            if (D4Button.Background != Brushes.Transparent)
                            {
                                ParseDeliverable(3);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                D4Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("Stack1"):
                            if (Stack1Button.Background != Brushes.Transparent)
                            {
                                ParseFeature(0);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                Stack1Button.Background = Brushes.Transparent;
                            }
                            break;
                        case ("Stack2"):
                            if (Stack2Button.Background != Brushes.Transparent)
                            {
                                ParseFeature(1);
                                player2.workersAvailable += 1;
                                player2.HasRetrieved = true;
                                player2.IsLocked = true;
                                player2.CurrentLocation = location;
                                Stack2Button.Background = Brushes.Transparent;
                            }
                            break;
                        default:
                            player2.PullWorkers(location);
                            player2.HasRetrieved = true;
                            player2.IsLocked = true;
                            player2.CurrentLocation = location;
                            break;
                    }
                }
            } 
        }

        public void ParseDeliverable(int position)
        {
            ResourceDecisionWindow window = new ResourceDecisionWindow(0, 0, deliverableDisplay[position], position, null, "deliverable");
            window.ShowDialog();

            if (isPlayer1Turn)
            {
                if (player1.GainedCard)
                {
                    if(deckHandler.DeliverableDeck.Count == 0)
                    {
                        EndGame();
                    }
                    else
                    {
                        Deliverable tempCard1 = deliverableDisplay[0];
                        Deliverable tempCard2 = deliverableDisplay[1];
                        Deliverable tempCard3 = deliverableDisplay[2];
                        Deliverable tempCard4 = deliverableDisplay[3];

                        deliverableDisplay.Clear();

                        switch (position)
                        {
                            case 0:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard3);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 1:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard3);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 2:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 3:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard3);
                                break;
                        }
                    }
                    
                }
            }
            else
            {
                if (player2.GainedCard)
                {
                    if (deckHandler.DeliverableDeck.Count == 0)
                    {
                        EndGame();
                    }
                    else
                    {
                        Deliverable tempCard1 = deliverableDisplay[0];
                        Deliverable tempCard2 = deliverableDisplay[1];
                        Deliverable tempCard3 = deliverableDisplay[2];
                        Deliverable tempCard4 = deliverableDisplay[3];

                        deliverableDisplay.Clear();

                        switch (position)
                        {
                            case 0:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard3);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 1:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard3);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 2:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard4);
                                break;
                            case 3:
                                deliverableDisplay.Add(deckHandler.DrawDeliverable());
                                deliverableDisplay.Add(tempCard1);
                                deliverableDisplay.Add(tempCard2);
                                deliverableDisplay.Add(tempCard3);
                                break;
                        }
                    }

                }
            }
        }


        public void ParseFeature(int stack)
        {
            ResourceDecisionWindow window = new ResourceDecisionWindow(0, 0, null, 0, featureDisplay[stack], "feature");
            window.ShowDialog();

            if (isPlayer1Turn)
            {
                if (player1.GainedCard)
                {
                    switch (stack)
                    {
                        case 0:
                            if(deckHandler.FeatureStackOne.Count == 0)
                            {
                                EndGame();
                            }
                            else
                            {
                                featureDisplay.Remove(featureDisplay[0]);
                                featureDisplay.Insert(0, deckHandler.DrawFeature(1));
                            }
                            break;
                        case 1:
                            if(deckHandler.FeatureStackTwo.Count == 0)
                            {
                                EndGame();
                            }
                            else
                            {
                                featureDisplay.Remove(featureDisplay[1]);
                                featureDisplay.Insert(1, deckHandler.DrawFeature(2));
                            }
                            break;
                    }
                }
            }
            else
            {
                if (player2.GainedCard)
                {
                    switch (stack)
                    {
                        case 0:
                            if (deckHandler.FeatureStackOne.Count == 0)
                            {
                                EndGame();
                            }
                            else
                            {
                                featureDisplay.Remove(featureDisplay[0]);
                                featureDisplay.Insert(0, deckHandler.DrawFeature(1));
                            }
                            break;
                        case 1:
                            if (deckHandler.FeatureStackTwo.Count == 0)
                            {
                                EndGame();
                            }
                            else
                            {
                                featureDisplay.Remove(featureDisplay[1]);
                                featureDisplay.Insert(1, deckHandler.DrawFeature(2));
                            }
                            break;
                    }
                }
            }
        }

        [STAThread]
        public void EndGame()
        {
            DialogResult dlg;

            dlg = System.Windows.Forms.MessageBox.Show(null, "The game is ending! Each resource is worth 1 point, each developer is worth 2 points, each experience is worth 3 points, and each obervation is worth 3 points", "End Game Scoring", MessageBoxButtons.OK, MessageBoxIcon.Information);

            player1.score += (player1.qualities + player1.interfaces + player1.constraints + player1.errorHandling + (player1.totalWorkers * 2) + (player1.Agriculture * 3) + (player1.Experience * 3));

            player2.score += (player2.qualities + player2.interfaces + player2.constraints + player2.errorHandling + (player2.totalWorkers * 2) + (player2.Agriculture * 3) + (player2.Experience * 3));

            UpdateBoard();

            if (player1.score > player2.score)
            {
                dlg = System.Windows.Forms.MessageBox.Show(null, $"Player 1 wins with a score of {player1.score}!!", "Congratulations", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Close();
            }
            else if (player1.score < player2.score)
            {
                dlg = System.Windows.Forms.MessageBox.Show(null, $"Player 2 wins with a score of {player2.score}!!", "Congratulations", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Close();
            }
            else
            {
                dlg = System.Windows.Forms.MessageBox.Show(null, $"Its a tied game with a score of {player1.score}?!", "Wow!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Close();
            }
        }

        public void SendPlayerReview()
        {
            if (isPlayer1Turn)
            {
                player1.Review();
            }
            else
            {
                player2.Review();
            }
        }


        #region Don't look..
        //This is to prevent having to do data binding. We can do that if you guys want
        //Dr. Bennett, please overlook this method. It is the source of our shame.
        public void UpdateBoard()
        {
            P1FoodLabel.Content = player1.workerLocation["WritingWorkshop"];
            P2FoodLabel.Content = player2.workerLocation["WritingWorkshop"];

            P1WoodLabel.Content = player1.workerLocation["Interviews"];
            P2WoodLabel.Content = player2.workerLocation["Interviews"];

            P1ClayLabel.Content = player1.workerLocation["MarketAnalysis"];
            P2ClayLabel.Content = player2.workerLocation["MarketAnalysis"];

            P1StoneLabel.Content = player1.workerLocation["Prototyping"];
            P2StoneLabel.Content = player2.workerLocation["Prototyping"];

            P1GoldLabel.Content = player1.workerLocation["Testing"];
            P2GoldLabel.Content = player2.workerLocation["Testing"];

            P1DevelopersLabel.Content = $"{player1.workersAvailable} / {player1.totalWorkers}";
            P2DevelopersLabel.Content = $"{player2.workersAvailable} / {player2.totalWorkers}";


            //
            P1ScoreLabel.Content = player1.score;
            P2ScoreLabel.Content = player2.score;

            P1UserStoriesLabel.Content = player1.userStories;
            P2UserStoriesLabel.Content = player2.userStories;

            P1QualitiesLabel.Content = player1.qualities;
            P2QualitiesLabel.Content = player2.qualities;

            P1ConstraintsLabel.Content = player1.constraints;
            P2ConstraintsLabel.Content = player2.constraints;

            P1InterfacesLabel.Content = player1.interfaces;
            P2InterfacesLabel.Content = player2.interfaces;

            P1ErrorHandlingLabel.Content = player1.errorHandling;
            P2ErrorHandlingLabel.Content = player2.errorHandling;

            P1ObservationTrackLabel.Content = player1.Agriculture;
            P2ObservationTrackLabel.Content = player2.Agriculture;

            P1ToolCountLabel.Content = $"{player1.AvailableExperience} / {player1.Experience}";
            P2ToolCountLabel.Content = $"{player2.AvailableExperience} / {player2.Experience}";

            //
            D1ResourceRewardLabel.Content = $"{deliverableDisplay[0].RewardAmount} {deliverableDisplay[0].ResourceReward}";
            D2ResourceRewardLabel.Content = $"{deliverableDisplay[1].RewardAmount} {deliverableDisplay[1].ResourceReward}";
            D3ResourceRewardLabel.Content = $"{deliverableDisplay[2].RewardAmount} {deliverableDisplay[2].ResourceReward}";
            D4ResourceRewardLabel.Content = $"{deliverableDisplay[3].RewardAmount} {deliverableDisplay[3].ResourceReward}";

            D1FeatureRewardLabel.Content = deliverableDisplay[0].PointValue;
            D2FeatureRewardLabel.Content = deliverableDisplay[1].PointValue;
            D3FeatureRewardLabel.Content = deliverableDisplay[2].PointValue;
            D4FeatureRewardLabel.Content = deliverableDisplay[3].PointValue;

            DeckCountLabel.Content = deckHandler.DeliverableDeck.Count;

            switch (featureDisplay[0].FeatureType)
            {
                case ("Normal"):
                    Stack1CostLabel.Content = $"{featureDisplay[0].QualityCost} Q, {featureDisplay[0].ConstraintCost} C, {featureDisplay[0].InterfaceCost} I, {featureDisplay[0].ErrorHandlingCost} E";
                    Stack1RewardLabel.Content = featureDisplay[0].PointValue;
                    break;
                case ("Any"):
                    Stack1CostLabel.Content = $"{featureDisplay[0].GeneralCost} of {featureDisplay[0].NumberDifferent} diff";
                    Stack1RewardLabel.Content = "Variable";
                    break;
                case ("Range"):
                    Stack1CostLabel.Content = $"{featureDisplay[0].MinimumResources} to {featureDisplay[0].MaximumResources} of any";
                    Stack1RewardLabel.Content = "Variable";
                    break;
            }

            switch (featureDisplay[1].FeatureType)
            {
                case ("Normal"):
                    Stack2CostLabel.Content = $"{featureDisplay[1].QualityCost} Q, {featureDisplay[1].ConstraintCost} C, {featureDisplay[1].InterfaceCost} I, {featureDisplay[1].ErrorHandlingCost} E";
                    Stack2RewardLabel.Content = featureDisplay[1].PointValue;
                    break;
                case ("Any"):
                    Stack2CostLabel.Content = $"{featureDisplay[1].GeneralCost} of {featureDisplay[1].NumberDifferent} diff";
                    Stack2RewardLabel.Content = "Variable";
                    break;
                case ("Range"):
                    Stack2CostLabel.Content = $"{featureDisplay[1].MinimumResources} to {featureDisplay[1].MaximumResources} of any";
                    Stack2RewardLabel.Content = "Variable";
                    break;
            }

            Stack1CountLabel.Content = deckHandler.FeatureStackOne.Count;
            Stack2CountLabel.Content = deckHandler.FeatureStackTwo.Count;

        }
        #endregion

        #endregion

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            if(gameManager.states[gameManager.currentState] == "Planning")
            {
                int setWorkers;

                if (isPlayer1Turn)
                {
                    switch (player1.CurrentLocation)
                    {
                        case ("WritingWorkshop"):
                            setWorkers = player1.workerLocation["WritingWorkshop"];
                            player1.workersAvailable += setWorkers;
                            player1.workerLocation["WritingWorkshop"] = 0;
                            break;
                        case ("Interviews"):
                            setWorkers = player1.workerLocation["Interviews"];
                            player1.workersAvailable += setWorkers;
                            player1.workerLocation["Interviews"] = 0;
                            break;
                        case ("MarketAnalysis"):
                            setWorkers = player1.workerLocation["MarketAnalysis"];
                            player1.workersAvailable += setWorkers;
                            player1.workerLocation["MarketAnalysis"] = 0;
                            break;
                        case ("Prototyping"):
                            setWorkers = player1.workerLocation["Prototyping"];
                            player1.workersAvailable += setWorkers;
                            player1.workerLocation["Prototyping"] = 0;
                            break;
                        case ("Testing"):
                            setWorkers = player1.workerLocation["Testing"];
                            player1.workersAvailable += setWorkers;
                            player1.workerLocation["Testing"] = 0;
                            break;
                        case ("Business"):
                            player1.workersAvailable += 1;
                            FarmButton.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("HiringFirm"):
                            player1.workersAvailable += 2;
                            HutButton.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("TrainingWorkshop"):
                            player1.workersAvailable += 1;
                            ToolsButton.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("D1"):
                            player1.workersAvailable += 1;
                            D1Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("D2"):
                            player1.workersAvailable += 1;
                            D2Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("D3"):
                            player1.workersAvailable += 1;
                            D3Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("D4"):
                            player1.workersAvailable += 1;
                            D4Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("Stack1"):
                            player1.workersAvailable += 1;
                            Stack1Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        case ("Stack2"):
                            player1.workersAvailable += 1;
                            Stack2Button.Background = Brushes.Transparent;
                            player1.CurrentLocation = String.Empty;
                            break;
                        default:
                            break;
                    }

                    player1.IsLocked = false;
                    player1.HasPlaced = false;

                }
                else
                {
                    switch (player2.CurrentLocation)
                    {
                        case ("WritingWorkshop"):
                            setWorkers = player2.workerLocation["WritingWorkshop"];
                            player2.workersAvailable += setWorkers;
                            player2.workerLocation["WritingWorkshop"] = 0;
                            break;
                        case ("Interviews"):
                            setWorkers = player2.workerLocation["Interviews"];
                            player2.workersAvailable += setWorkers;
                            player2.workerLocation["Interviews"] = 0;
                            break;
                        case ("MarketAnalysis"):
                            setWorkers = player2.workerLocation["MarketAnalysis"];
                            player2.workersAvailable += setWorkers;
                            player2.workerLocation["MarketAnalysis"] = 0;
                            break;
                        case ("Prototyping"):
                            setWorkers = player2.workerLocation["Prototyping"];
                            player2.workersAvailable += setWorkers;
                            player2.workerLocation["Prototyping"] = 0;
                            break;
                        case ("Testing"):
                            setWorkers = player2.workerLocation["Testing"];
                            player2.workersAvailable += setWorkers;
                            player2.workerLocation["Testing"] = 0;
                            break;
                        case ("Business"):
                            player2.workersAvailable += 1;
                            FarmButton.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("HiringFirm"):
                            player2.workersAvailable += 2;
                            HutButton.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("TrainingWorkshop"):
                            player2.workersAvailable += 1;
                            ToolsButton.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("D1"):
                            player2.workersAvailable += 1;
                            D1Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("D2"):
                            player2.workersAvailable += 1;
                            D2Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("D3"):
                            player2.workersAvailable += 1;
                            D3Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("D4"):
                            player2.workersAvailable += 1;
                            D4Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("Stack1"):
                            player2.workersAvailable += 1;
                            Stack1Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        case ("Stack2"):
                            player2.workersAvailable += 1;
                            Stack2Button.Background = Brushes.Transparent;
                            player2.CurrentLocation = String.Empty;
                            break;
                        default:
                            break;
                    }

                    player2.IsLocked = false;
                    player2.HasPlaced = false;

                }
            }

            UpdateBoard();
        }

       
    }
}
