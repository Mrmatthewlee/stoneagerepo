﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    /**
* Class Name: Location <br>
* Class Purpose: Implement the GainResource method for the Interview location where the player gains qualities<br>
*
* <hr>
* Date created: 6/19/2020 <br>
* @author Darien Roach
*/
    class Interviews : ILocation
    {
        public int DeveloperCount { get; set; }
        public int RESOURCE_DIVISOR { get; private set; }

        /**
 * Method Name: Interviews<br>
 * Method Purpose: Default constructor for the Interviews class, collecting the developer count from the player and establishing the resource divisor constant <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @param  developerCount - used to instantiated the value of the class's own DeveloperCount property
 */
        public Interviews(int developerCount)
        {
            DeveloperCount = developerCount;
            RESOURCE_DIVISOR = 3;
        }

        /**
* Method Name: GainResource<br>
* Method Purpose: Returns the amount of Qualities the player obtains after randomly generating values between 1-6 and summing the total <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @return total amount of qualities gained
*/
        public int GainResource(int experienceBonus)
        {
            int qualities;
            int sum = experienceBonus;

            Random rng = new Random();

            for (int i = 0; i < DeveloperCount; i++)
            {
                sum += rng.Next(1, 7);
            }

            qualities = sum / RESOURCE_DIVISOR;

            return qualities;
        }
    }
}
