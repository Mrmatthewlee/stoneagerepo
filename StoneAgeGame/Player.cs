﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoneAgeGame
{
    //Has to stay public
    public class Player 
    {
        public int score { get; set; }

        public int totalWorkers { get; set; }
        public int workersAvailable { get; set; }

        public bool HasStartPlayerToken { get; set; }

        public bool IsLocked { get; set; }
        public string CurrentLocation { get; set; }
        public bool HasPlaced { get; set; }
        public bool HasRetrieved { get; set; }
        public bool GainedCard { get; set; }

        public int userStories { get; set; }
        public int UnfedWorkers { get; set; }
        public int qualities { get; set; }
        public int constraints { get; set; }
        public int interfaces { get; set; }
        public int errorHandling { get; set; }
        public int Agriculture { get; set; }
        public int Experience { get; set; }
        public int AvailableExperience { get; set; }
        public int ExperienceToBeApplied { get; set; }
        public int SCORE_PENALTY { get; private set; }
        private ILocation Location { get; set; }

        private DialogResult? dlg = null;

        
        //Dictionary that contains the number of workers at each location ("location", number of workers)
        //Beginning at 0
        
        public Dictionary<string, int> workerLocation = new Dictionary<string, int>
        {
            {"WritingWorkshop", 0},
            {"Interviews", 0},
            {"MarketAnalysis", 0},
            {"Prototyping", 0},
            {"Testing", 0},
            {"Business", 0 },
            {"HiringFirm", 0 },
            {"TrainingWorkshop", 0 },
            {"D1", 0 },
            {"D2", 0 },
            {"D3", 0 },
            {"D4", 0 },
            {"Stack1", 0 },
            {"Stack2", 0 }
        };
        
        public Player()
        {
            totalWorkers = 5;       //Start the players off with 5 total and available workers
            workersAvailable = 5;
            SCORE_PENALTY = -10;
        }

        public void AssignWorker(string location)
        {
            workersAvailable--;     //Lower available workers by 1
            workerLocation[location] += 1;

            //int count;

            //workerLocation.TryGetValue(location, out count);    //Get the count of workers at "location"
            //workerLocation[location] = count + 1;               //Increment the count of workers at "location"
        }

        public void PullWorkers(string location)
        {
            switch (location)
            {
                case "WritingWorkshop":
                    GainUserStories(workerLocation[location]);
                    break;
                case "Interviews":
                    GainQualities(workerLocation[location]);
                    break;
                case "MarketAnalysis":
                    GainConstraints(workerLocation[location]);
                    break;
                case "Prototyping":
                    GainInterfaces(workerLocation[location]);
                    break;
                case "Testing":
                    GainErrorHandling(workerLocation[location]);
                    break;
            }
        }

        #region Phase 3 - Sprint Review

        /**
 * Method Name: Review <br>
 * Method Purpose: Runs through the processes for Phase 3 (Sprint Review) through 3 distinct methods <br>
 *
 * <hr>
 * Date created: 6/21/2020 <br>
 *
 * <hr>
 *
 * <hr>
 */
        public void Review()
        {
            ObservationGain();
            FeedDevelopers();
            SetStartPlayer();
            AvailableExperience = Experience;
        }

        /**
 * Method Name: ObservationGain <br>
 * Method Purpose: Gains user stories for the player based on the player's position on the Observation track <br>
 *
 * <hr>
 * Date created: 6/21/2020 <br>
 *
 * <hr>
 *
 * <hr>
 */
        public void ObservationGain()
        {
            userStories += Agriculture;
        }

        /**
 * Method Name: FeedDevelopers <br>
 * Method Purpose: Allocates user stories and other resources in order to feed developers, dropping the player's score by 10 if they are unable to feed their developers <br>
 *
 * <hr>
 * Date created: 6/21/2020 <br>
 *
 * <hr>
 *
 * <hr>
 */
        [STAThread]
        public void FeedDevelopers()
        {
            if (userStories < totalWorkers)
            {
                int totalResources = qualities + interfaces + constraints + errorHandling;
                UnfedWorkers = totalWorkers - userStories;

                userStories = 0;

                if (totalResources < UnfedWorkers)
                {
                    score += SCORE_PENALTY;
                }
                else
                {
                    int resourceCollectionAmount = UnfedWorkers;

                    if (totalResources < resourceCollectionAmount)
                    {
                        //Cant pay so incur penalty
                        score += SCORE_PENALTY;
                        UnfedWorkers = 0;
                    }
                    else
                    {
                        //Open the resource decision window and pass the resource collection amount
                        ResourceDecisionWindow window = new ResourceDecisionWindow(resourceCollectionAmount, 0, null, 0, null, "feeding");
                        window.ShowDialog();
                        UnfedWorkers = 0;
                    }
                    
                    /*
                    dlg = MessageBox.Show(null, "Will you expend resources in place of user stories in order to fulfill developer requirements?", "Resource Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dlg == DialogResult.No || dlg == DialogResult.Cancel)
                    {
                        score += SCORE_PENALTY;
                    }
                    else
                    {
                        ResourceAllocation(unfedWorkers);
                    }
                    */
                }
            }
            else
            {
                userStories -= totalWorkers;
            }
        }

        public void SpendFeedingResources(int qualitiesSpent, int constraintsSpent, int interfacesSpent, int errorHandlingSpent)
        {
            int total = qualitiesSpent + constraintsSpent + interfacesSpent + errorHandlingSpent;

            qualities -= qualitiesSpent;
            constraints -= constraintsSpent;
            interfaces -= interfacesSpent;
            errorHandling -= errorHandlingSpent;

            if(total < UnfedWorkers)
            {
                score -= SCORE_PENALTY;
            }
        }

        public void SpendGeneralResources(int qualitiesSpent, int constraintsSpent, int interfacesSpent, int errorHandlingSpent)
        {
            qualities -= qualitiesSpent;
            constraints -= constraintsSpent;
            interfaces -= interfacesSpent;
            errorHandling -= errorHandlingSpent;
        }
        /**
 * Method Name: ResourceAllocation <br>
 * Method Purpose: Handles the logic involved in determining what resources the player wishes to spend in place of user stories <br>
 *
 * <hr>
 * Date created: 6/21/2020 <br>
 *
 * <hr>
 *
 * <hr>
 */
        [STAThread]
        public void ResourceAllocation(int unfedWorkers)
        {
            if (unfedWorkers > 0 && qualities > 0)
            {
                dlg = MessageBox.Show(null, "Will you expend your qualities?", "Resource Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dlg == DialogResult.Yes)
                {
                    if (qualities >= unfedWorkers)
                    {
                        qualities -= unfedWorkers;
                        unfedWorkers = 0;
                    }
                    else
                    {
                        unfedWorkers -= qualities;
                        qualities = 0;
                    }
                }
            }

            if (unfedWorkers > 0 && interfaces > 0)
            {
                dlg = MessageBox.Show(null, "Will you expend your constraints?", "Resource Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dlg == DialogResult.Yes)
                {
                    if (interfaces >= unfedWorkers)
                    {
                        interfaces -= unfedWorkers;
                        unfedWorkers = 0;
                    }
                    else
                    {
                        unfedWorkers -= interfaces;
                        interfaces = 0;
                    }
                }
            }

            if (unfedWorkers > 0 && constraints > 0)
            {
                dlg = MessageBox.Show(null, "Will you expend your interfaces?", "Resource Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dlg == DialogResult.Yes)
                {
                    if (constraints >= unfedWorkers)
                    {
                        constraints -= unfedWorkers;
                        unfedWorkers = 0;
                    }
                    else
                    {
                        unfedWorkers -= constraints;
                        constraints = 0;
                    }
                }
            }

            if (unfedWorkers > 0 && errorHandling > 0)
            {
                dlg = MessageBox.Show(null, "Will you expend your error handling?", "Resource Allocation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dlg == DialogResult.Yes)
                {
                    if (errorHandling >= unfedWorkers)
                    {
                        errorHandling -= unfedWorkers;
                        unfedWorkers = 0;
                    }
                    else
                    {
                        unfedWorkers -= errorHandling;
                        errorHandling = 0;
                    }
                }
            }

            if (unfedWorkers == 0)
            {
                score += SCORE_PENALTY;
            }
        }

        /**
 * Method Name: SetStartPlayer <br>
 * Method Purpose: Sets whether or not the player has the player has the start player token <br>
 *
 * <hr>
 * Date created: 6/21/2020 <br>
 *
 * <hr>
 *
 * <hr>
 */
        public void SetStartPlayer()
        {
            if (HasStartPlayerToken == true)
            {
                HasStartPlayerToken = false;
            }
            else
            {
                HasStartPlayerToken = true;
            }
        }
        #endregion

        #region Location Logic

        /**
 * Method Name: GainUserStories<br>
 * Method Purpose: gain user stories by running through the logic for the writing workshop location <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @param  assignedDevelopers - number of developers at the location
 */
        public void GainUserStories(int assignedDevelopers)
        {
            if (assignedDevelopers > 0)
            {
                WritingWorkshop workshop = new WritingWorkshop(assignedDevelopers);
                Location = workshop;

                int experienceBonus = ApplyExperience();
                userStories += Location.GainResource(experienceBonus);
                workerLocation["WritingWorkshop"] = 0;
                workersAvailable += assignedDevelopers;
            }
        }

        /**
* Method Name: GainQualities<br>
* Method Purpose: gain qualities by running through the logic for the interviews location <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @param  assignedDevelopers - number of developers at the location
*/
        public void GainQualities(int assignedDevelopers)
        {
            if (assignedDevelopers > 0)
            {
                Interviews interview = new Interviews(assignedDevelopers);
                Location = interview;

                int experienceBonus = ApplyExperience();
                qualities += Location.GainResource(experienceBonus);
                workerLocation["Interviews"] = 0;
                workersAvailable += assignedDevelopers;
            }
        }

        /**
* Method Name: GainConstraints<br>
* Method Purpose: gain constraints by running through the logic for the market analysis location <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @param  assignedDevelopers - number of developers at the location
*/
        public void GainConstraints(int assignedDevelopers)
        {
            if (assignedDevelopers > 0)
            {
                MarketAnalysis market = new MarketAnalysis(assignedDevelopers);
                Location = market;

                int experienceBonus = ApplyExperience();
                constraints += Location.GainResource(experienceBonus);
                workerLocation["MarketAnalysis"] = 0;
                workersAvailable += assignedDevelopers;
            }
        }

        /**
* Method Name: GainInterfaces<br>
* Method Purpose: gain interfaces by running through the logic for the prototyping location <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @param  assignedDevelopers - number of developers at the location
*/
        public void GainInterfaces(int assignedDevelopers)
        {
            if (assignedDevelopers > 0)
            {
                Prototyping prototype = new Prototyping(assignedDevelopers);
                Location = prototype;

                int experienceBonus = ApplyExperience();
                interfaces += Location.GainResource(experienceBonus);
                workerLocation["Prototyping"] = 0;
                workersAvailable += assignedDevelopers;
            }
        }

        /**
* Method Name: GainErrorHandling<br>
* Method Purpose: gain error handling by running through the logic for the testing location <br>
*
* <hr>
* Date created: 6/19/2020 <br>
*
* <hr>
*
* <hr>
*   @param  assignedDevelopers - number of developers at the location
*/
        public void GainErrorHandling(int assignedDevelopers)
        {
            if (assignedDevelopers > 0)
            {
                Testing test = new Testing(assignedDevelopers);
                Location = test;

                int experienceBonus = ApplyExperience();
                errorHandling += Location.GainResource(experienceBonus);
                workerLocation["Testing"] = 0;
                workersAvailable += assignedDevelopers;
            }
        }

 
 

        /**
 * Method Name: GainResource <br>
 * Method Purpose: Gain a number of some resource based upon the specific contextualized location <br>
 *
 * <hr>
 * Date created: 6/19/2020 <br>
 *
 * <hr>
 *
 * <hr>
 *   @return number of the resource gained
 */

        public int GainResource(int experienceBonus)
        {
            int resourceNumber = Location.GainResource(experienceBonus);

            
            return resourceNumber;
        }


        public int ApplyExperience()
        {
            int experienceUsed = 0;

            if (AvailableExperience > 0)
            {
                //Open the resource decision window and pass the resource collection amount
                ResourceDecisionWindow window = new ResourceDecisionWindow(0, AvailableExperience, null, 0, null, "tools");
                window.ShowDialog();
                experienceUsed = ExperienceToBeApplied;
                AvailableExperience -= experienceUsed;
                ExperienceToBeApplied = 0;
                return experienceUsed;
            }

            return experienceUsed;

        }

        #endregion
    }


}
