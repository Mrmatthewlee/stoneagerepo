﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StoneAgeGame
{
    class GameManager
    {
        public int currentState { get; set; }
        public int turnNumber { get; set; }

        //List of all game states
        public readonly string[] states = { "Planning", "Planning", "Execution", "Execution", "Review", "Review" };

        //Player player1 = new Player();
        //Player player2 = new Player();

        //private static MainWindow instance = new MainWindow();

        public GameManager()
        {
            currentState = 0;
        }

        //Return the next game state to MainWindow
        public void NextPhase()
        {
            switch (currentState)
            {
                case 0:
                    currentState = 2;
                    break;
                case 1:
                    currentState = 2;
                    break;
                case 2:
                    currentState = 4;
                    break;
                case 3:
                    currentState = 4;
                    break;
                case 4:
                    currentState = 0;
                    break;
                case 5:
                    currentState = 0;
                    break;
            }
        }
          

        public void NextTurn()
        {
            turnNumber++;
        }
    }
}
