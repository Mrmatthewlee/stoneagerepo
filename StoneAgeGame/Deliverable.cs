﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    public class Deliverable
    {


        public string ResourceReward { get; set; }
        public int RewardAmount { get; set; }
        public int PointValue { get; set; }
        public int Position { get; set; }

        private const int TOTAL_REWARDS = 6;

        //Must be static to be shared across all objects of this class.
        //This prevents the random number being the same when creating
        //multiple objects at once.
        static Random rng = new Random();

        public Deliverable()
        {
            switch (rng.Next(1, 5))
            {
                case (1):
                    ResourceReward = "Qualities";
                    break;
                case (2):
                    ResourceReward = "Constraints";
                    break;
                case (3):
                    ResourceReward = "Interfaces";
                    break;
                case (4):
                    ResourceReward = "Error Handling";
                    break;
            }

            GenerateRewards();

        }

        private void GenerateRewards()
        {
            int rewardCounter = rng.Next(1, 7);

            RewardAmount = rewardCounter;

            PointValue = TOTAL_REWARDS - RewardAmount;
        }

    }
}
