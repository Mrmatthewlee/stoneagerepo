﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    class DeckHandler
    {
        private List<Feature> FeatureDeck { get; set; }
        public List<Deliverable> DeliverableDeck { get; set; }
        public List<Feature> FeatureStackOne { get; set; }
        public List<Feature> FeatureStackTwo { get; set; }

        static Random rng = new Random();

        private const int INITIAL_FEATURE_DECK_SIZE = 28;
        private const int INITIAL_DELIVERABLE_DECK_SIZE = 36; //36


        public DeckHandler()
        {
            FeatureDeck = new List<Feature>();
            DeliverableDeck = new List<Deliverable>();
            FeatureStackOne = new List<Feature>();
            FeatureStackTwo = new List<Feature>();

            GenerateDeliverableDeck();
            GenerateFeatureDeck();

            //ShuffleFeatures();
            SplitFeatureDeck();

            //ShuffleDeliverables();
        }

        public Feature CreateFeature()
        {
            
            Feature feature = null;

            int typeChance = rng.Next(0, 100);

            if (typeChance >= 25)
            {
                feature = new Feature("Normal");
            }
            else if (typeChance >= 5)
            {
                feature = new Feature("Any");
            }
            else
            {
                feature = new Feature("Range");
            }

            return feature;
        }

        public void GenerateFeatureDeck()
        {
            for (int i = 0; i < INITIAL_FEATURE_DECK_SIZE; i++)
            {
                FeatureDeck.Add(CreateFeature());
            }
        }

        public Deliverable CreateDeliverable()
        {
            Deliverable deliverable = new Deliverable();
            return deliverable;
        }

        public void GenerateDeliverableDeck()
        {
            for (int i = 0; i < INITIAL_DELIVERABLE_DECK_SIZE; i++)
            {
                DeliverableDeck.Add(CreateDeliverable());
            }
        }

        public void ShuffleDeliverables()
        {
            

            List<Deliverable> newDeck = new List<Deliverable>(DeliverableDeck.Count);

            int newIndex = 0;
            for (int i = 0; i < DeliverableDeck.Count; i++)
            {
                newIndex = rng.Next(0, DeliverableDeck.Count);
                newDeck.Insert(newIndex, DeliverableDeck[i]);
            }

            DeliverableDeck.Clear();

            foreach (Deliverable deliverable in newDeck)
            {
                DeliverableDeck.Add(deliverable);
            }
        }

        public void ShuffleFeatures()
        {
            

            List<Feature> newDeck = new List<Feature>(FeatureDeck.Count);

            int newIndex = 0;
            for (int i = 0; i < DeliverableDeck.Count; i++)
            {
                newIndex = rng.Next(0, FeatureDeck.Count);
                newDeck.Insert(newIndex, FeatureDeck[i]);
            }

            FeatureDeck.Clear();

            foreach (Feature feature in newDeck)
            {
                FeatureDeck.Add(feature);
            }
        }

        public void SplitFeatureDeck()
        {
            for (int i = 0; i < FeatureDeck.Count; i++)
            {
                if (i < 14)
                {
                    FeatureStackOne.Add(FeatureDeck[i]);
                }
                else
                {
                    FeatureStackTwo.Add(FeatureDeck[i]);
                }
            }
        }

        public Deliverable DrawDeliverable()
        {
            Deliverable drawnDeliverable = DeliverableDeck[0];
            DeliverableDeck.Remove(DeliverableDeck[0]);

            return drawnDeliverable;
        }

        public Feature DrawFeature(int stackNumber)
        {
            Feature drawnFeature = null;
            switch (stackNumber)
            {
                case (1):
                    drawnFeature = FeatureStackOne[0];
                    FeatureStackOne.Remove(FeatureStackOne[0]);
                    break;
                case (2):
                    drawnFeature = FeatureStackTwo[0];
                    FeatureStackTwo.Remove(FeatureStackTwo[0]);
                    break;
            }
            return drawnFeature;
        }
    }
}
