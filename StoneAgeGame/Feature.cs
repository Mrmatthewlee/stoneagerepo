﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoneAgeGame
{
    public class Feature
    {
        public int QualityCost { get; set; }
        public int ConstraintCost { get; set; }
        public int InterfaceCost { get; set; }
        public int ErrorHandlingCost { get; set; }
        public int GeneralCost { get; set; }
        public int NumberDifferent { get; set; }
        public int MinimumResources { get; set; }
        public int MaximumResources { get; set; }
        public int PointValue { get; set; }
        public int Position { get; set; }

        public string FeatureType { get; set; }

        public int TOTAL_COST { get; private set; }


        private const int QUALITY_VALUE = 3;
        private const int CONSTRAINT_VALUE = 4;
        private const int INTERFACE_VALUE = 5;
        private const int ERROR_HANDLING_VALUE = 6;

        static Random rng = new Random();

        public Feature()
        {
            FeatureType = "Normal";

            QualityCost = 0;
            ConstraintCost = 0;
            InterfaceCost = 0;
            ErrorHandlingCost = 0;

            TOTAL_COST = 4;

            GenerateNormalFeature();
            CalculatePointValue();

        }

        public Feature(string type)
        {
            FeatureType = type;

            switch (FeatureType)
            {
                case ("Normal"):
                    QualityCost = 0;
                    ConstraintCost = 0;
                    InterfaceCost = 0;
                    ErrorHandlingCost = 0;

                    
                    if (rng.Next(0, 2) == 0)
                    {
                        TOTAL_COST = 3;
                    }
                    else
                    {
                        TOTAL_COST = 4;
                    }

                    GenerateNormalFeature();
                    CalculatePointValue();
                    break;
                case ("Any"):
                    GenerateAnyFeature();
                    break;
                case ("Range"):
                    GenerateRangeFeature();
                    break;
            }
        }


        public void GenerateNormalFeature()
        {
            

            int sum = 0;
            int intermediary = 0;
            while (sum < TOTAL_COST)
            {
                if (sum < TOTAL_COST)
                {
                    intermediary = rng.Next(0, 3);
                    sum += intermediary;
                    if (sum <= TOTAL_COST)
                    {
                        QualityCost += intermediary;
                    }
                    else
                    {
                        sum -= intermediary;
                    }
                }

                if (sum < TOTAL_COST)
                {
                    intermediary = rng.Next(0, 2);
                    sum += intermediary;
                    if (sum <= TOTAL_COST)
                    {
                        ConstraintCost += intermediary;
                    }
                    else
                    {
                        sum -= intermediary;
                    }
                }

                if (sum < TOTAL_COST)
                {
                    intermediary = rng.Next(0, 2);
                    sum += intermediary;
                    if (sum <= TOTAL_COST)
                    {
                        InterfaceCost += intermediary;
                    }
                    else
                    {
                        sum -= intermediary;
                    }
                }

                if (sum < TOTAL_COST)
                {
                    intermediary = rng.Next(0, 1);
                    sum += intermediary;
                    if (sum <= TOTAL_COST)
                    {
                        ErrorHandlingCost += intermediary;
                    }
                    else
                    {
                        sum -= intermediary;
                    }
                }
            }
        }

        public void CalculatePointValue()
        {
            PointValue = (QualityCost * QUALITY_VALUE) + (ConstraintCost * CONSTRAINT_VALUE) + (InterfaceCost * INTERFACE_VALUE) + (ErrorHandlingCost * ERROR_HANDLING_VALUE);
        }

        public void GenerateAnyFeature()
        {
           
            NumberDifferent = rng.Next(2, 4);

            GeneralCost = rng.Next(4, 7);

            TOTAL_COST = GeneralCost;
        }

        public void GenerateRangeFeature()
        {

            MinimumResources = rng.Next(1, 3);

            MaximumResources = rng.Next(6, 9);
        }
    }
}
